const e = require('express');
var express = require('express');
var router = express.Router();
var fs = require('fs')
/* GET users listing. */
router.post('/', function (req, res, next) {
  var lastId;
  fs.readFile('./mock/todo.json', function (err, data) {
    if (err) {
      console.log(err);
    } else {
      let todoList = data.toString();	//将Buffer转换成字符串
      todoList = JSON.parse(data);		//将数据转换为 JavaScript对象。
      console.log("length:" + todoList.data.length)
      var id;
      //如果文件中没有json对象，则一开始的id为0
      if (todoList.data.length == 0) {
        id = 0;
      } else {
        id = todoList.data[todoList.data.length - 1].id
      }
      //需要添加的Id
      lastId = id + 1;
      var todo = {
        "id": lastId,
        "todo": req.body.todo
      }
      todoList.data.push(todo);	//我所得到的数据里todoList是一个数组，所以直接push进去
      let new_data = JSON.stringify(todoList);	//将json对象转化为字符处才能存储进去
      fs.writeFile('./mock/todo.json', new_data, function (err) {
        if (err) {
          console.log(err);
        } else {
          console.log("数据添加成功！");
        }
      })
    }
  });
  //延时200毫秒，同步读文件
  setTimeout(function () {
    var todoList = fs.readFileSync('./mock/todo.json', function (err, data) {
      if (err) {
        console.error(err);
      }
      var todo = data.toString();
      todo = JSON.parse(todo);
      return todo;
    })
    console.log("todoList:" + todoList)
    var todo = JSON.parse(todoList);
    res.send(todo.data);
  }, 200)
});

module.exports = router;
