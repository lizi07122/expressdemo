var express = require('express');
var router = express.Router();
var fs = require('fs')
router.post("/", function (req, res, next) {
  var id = req.body.id
  fs.readFile('./mock/todo.json', function (err, data) {
    if (err) {
      console.log(err);
    } else {
      let todoList = data.toString();	//将Buffer转换成字符串
      todoList = JSON.parse(data);		//将数据转换为 JavaScript对象。 
      for (var i = 0; i < todoList.data.length; i++) {
        if (id == todoList.data[i].id) {
          todoList.data.splice(i, 1)//当传进来的id等于json对象的id，就把这个json对象从数组中删除
        }
      }
      let new_data = JSON.stringify(todoList);	//将json对象转化为字符处才能存储进去
      fs.writeFile('./mock/todo.json', new_data, function (err) {
        if (err) {
          console.log(err);
        } else {
          console.log("数据删除成功！");
        }
      })
    }
  });
  //延时200毫秒，异步读文件
  setTimeout(function () {
    var todoList = fs.readFileSync('./mock/todo.json', function (err, data) {
      if (err) {
        console.error(err);
      }
      var todo = data.toString();
      todo = JSON.parse(todo);
      return todo;
    })
    console.log("todoList:" + todoList)
    var todo = JSON.parse(todoList);
    res.send(todo.data);
  }, 200)
});

module.exports = router;