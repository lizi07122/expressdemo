const e = require('express');
var express = require('express');
var router = express.Router();
var fs = require('fs');
const { setTimeout } = require('timers');
/* GET home page. */
router.get('/', function(req, res, next) {
  //先异步读文件，文件为空的话就默认创建一个json数组
  var todoList=fs.readFile('./mock/todo.json',function(err,data){
    if(err){
        console.error(err);
    }
    var todo = data.toString();
    if(todo==""){
      var newTodo={
        "data":[
          
        ]
      }
      let new_data=JSON.stringify(newTodo);
      fs.writeFile('./mock/todo.json', new_data, function (err) {
        if (err) {
          console.log(err);
        } 
      })
    }
  })
  //延时200毫秒，同步读文件
  setTimeout(function(){
    var todoList=fs.readFileSync('./mock/todo.json',function(err,data){
      if(err){
          console.error(err);
      }
      var todo = data.toString();
      todo = JSON.parse(todo);
      return todo;
    })
    console.log("todoList:"+todoList)
    var todo=JSON.parse(todoList); 
    res.render('index', { title: 'Express',datas:todo.data});
  },200)
});


module.exports = router;
